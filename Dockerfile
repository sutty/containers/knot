FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache knot knot-prometheus-exporter daemonize
COPY ./monit.conf /etc/monit.d/knot.conf
COPY ./prometheusd.sh /usr/local/bin/prometheusd

EXPOSE 53
EXPOSE 53/udp
EXPOSE 5000

VOLUME /var/lib/knot
