#!/bin/sh
set -e

STATS=${STATS:-/tmp/stats.yaml}

case $1 in
  start)
    rm -f /tmp/prometheus.pid
    daemonize -c /tmp -p /tmp/prometheus.pid -l /tmp/prometheus.pid -u knot /usr/bin/knot-prometheus-exporter ${STATS}
    ;;
  stop) cat /tmp/prometheus.pid | xargs -r kill ;;
esac
